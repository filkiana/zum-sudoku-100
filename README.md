Readme
To je dokumentace pro program “Sudoku 100%”.
Prosim se divat ve "Display source" formatu.

 
1.Popis.

    a. To je program, ktery resi sudoku NxN dvema zpusoby. 
    Prvni s pomoci CSP a jiny s pomoci SAT.

    b. CSP. Vytvori promennou pro kazdou bunku desky, s domenou 
    rovnou [1..size2 + 1], pokud deska ma 0 na teto pozici. Value se rovna i, 
    pokud ma deska v dane bunce cislo i. Potom vytvari omezeni pro kazdu bunku. 
    Pak v DFSu leda spravne reseni. Nejdrive kontroluje omezeni a overuje je-li 
    moznost  dostat nejake hodnoty pro bunky. Potom kdyz uz neni ani jednou 
    zmeny, zacina náhodně vybirat hodnoty z mnoziny moznych hodnot a zase 
    kontroluje omezeni. Kdyz dojde k chybe vybira dalsi hodnotu. A tak dal. 
    Kdyz najde reseni vrati ho.

    c. SAT. Kazda bunka a jeji hodnota bude zapsana jak promnena Xijv. 
    To znamena ze kdyz Xijv je pravda pak v bunce s indexy i a j bude hodnota v.
    Vsechny pravidla prevede do logickych formuli(napr. Kdyz X118 je pravda pak 
    X148 neni atp). Pak SAT solver resi mnozinu tech formuli a vrati spravny vysledek.

 

2. Zavislosti.

    a. Programovaci jazyk – Python 3

    b. Pouzite knihovny:

                        i. Time

                        ii. Pycosat

3. Documentace.

    a. main.py

            i. solve (sudoku, size)

                1. Prijima sudoku tabulku jak seznam seznamu cisel velikosti 
                size2 * size2 a hodnotami mezi 1 a size2 + 1(napr. Pro normalni 
                9x9 sudoku hodnoty musi byt mezi 1 a 9)
                
                2. Resi sudoku, pise reseni a jak dlouho a jaky zpusob to vyresil.

    b. sat.py

            i. solve (sudoku, size)

                1. Prijima sudoku tabulku jak seznam seznamu cisel velikosti 
                size2 * size2 a hodnotami mezi 1 a size2 + 1(napr. Pro normalni 
                9x9 sudoku hodnoty musi byt mezi 1 a 9)

                2. Pomoci pycoSAT resi klauzuli

                3. Vrati tabulku sudoku ve stejnem formatu

            ii. sudokuClauses(size)

                1. Prijima jenom vvelikost size.

                2. Vytvori seznam klauzuli pro sudoku

                3. Vrati seznam klauzuli

            iii. haveValueClauses(clauses, size):

                1. Prijima seznam klauzuli a velikost sudoku.

                2. Pridava do seznamu ze kazda bunka musi mit hodnotu

            iv. rowsClauses(clauses, size)

                1. Prijima seznam klauzuli a velikost sudoku.

                2. Pridava do seznamu ze kazda bunka v radku musi mit vlastni hodnotu

            v. colsClauses(clauses, size)

                1. Prijima seznam klauzuli a velikost sudoku.

                2. Pridava do seznamu ze kazda bunka ve sloupci musi mit 
                vlastni hodnotu

 
            vi. areaClauses(clauses, size)

                1. Prijima seznam klauzuli a velikost sudoku.

                2. Pridava do seznamu ze kazda bunka ve vetsi bunce musi mit 
                vlastni hodnotu

            vii. Valid (cells, clauses, size)

                1. Prijima seznam indexu bunek, seznam klauzuli a velikost sudoku.

                2. Overuje ze v ruznych bunkach jsou ruzne hodnoty.

            viii. initClauses(clauses, sudoku, size)

                1. Prijima seznam klauzuli, tabulku sudoku (ve stejnem formatu 
                jak drive) a velikost.

                2. Pridava do seznamu zname hodnoty ze sudoku.

            ix. getValue(i, j, solution, size):

                1. Prijima indexy bunky, reseni a velikost.

                2. Vrati hodnotu bunky podle reseni

            x. getSolved(sudoku, solution, size)

                1. Prijima tabulku sudoku (ve stejmen formatu jak drive), r
                eseni z pycosatu a velikost.

                2. Vrati vyresenou tabulku sudoku ( ve stejnem formatu)

    c. csp.py

            i. solve(sudoku, size)

                1. Priima sudoku tabulku jak seznam seznamu cisel velikosti 
                size2 * size2 a hodnotami mezi 1 a size2 + 1(napr. Pro normalni 
                9x9 sudoku hodnoty musi byt mezi 1 a 9)

                2. Vrati reseni ve formatu csp_sudoku(seznam  objectu tridy CSPSudokuCell)

            ii. initCsp(sudoku, size)

                1. Prijima tabulku sudoku a velikost

                2. Prevadi Sudoku ze seznamu seznamu cisel do seznamu seznamu 
                objektu tridy CSPSudokuCell

                3. Vrati do seznam seznamu objektu tridy CSPSudokuCell

            iii. controlContraints (csp_sudoku, size)

                1. Prijima tabulku CSPSudokuCell a velikost

                2. Kontroluje omezeni. Prestava prochazet kdy neprovedlo ani 
                jednou zmeny.

            iv.     DFS (sudoku, size)

                1. Prijima tabulku sudoku a velikost

                2. Pomoci DFS hleda sprvne reseni. Hleda bunku ve kterou bude 
                nejmin moznosti pro vyber a vybira hodnoty ze CSPSudokuCell.domain 
                (mnozina moznych hodnot). Vybrannou hodnotu považuje jak 
                spravnou. Potom kdy dojde do chyby pak vybira dalsi hodnotu.

                3. Vrati tabulku CSPSudokuCell

            v. findLeast(csp_sudoku, size)

                1. Prijima tabulku CSPSudokuCell a velikost

                2. Hleda bunku ve kterou bude nejmin moznosti pro vyber.

            vi. isSolution(csp_sudoku, size)

                1. Prijima tabulku CSPSudokuCell a velikost

                2. Overi je-li soucet vsech bunek spravny nebo ne

                3. Vrati True nebo False

            vii. printCSPSudoku(csp_sudoku, size)

                1. Prijima tabulku CSPSudokuCell a velikost

                2. Vypise tabulku CSPSudokuCell

            viii. printSudoku(sudoku, size)

                1. Prijima tabulku sudoku a velikost

                2. Vypise tabulku sudoku

            ix. isError(csp_sudoku)

                1. Prijima tabulku CSPSudokuCell

                2. Overi ze v kazdou bunce u kterou nevime hodnotu velikost 
                moznych hodnot se nerovna 0.

                3. Vrati True nebo False

 

    d. CSPSudokuCell.py

            i. CSPSudokuCell je tridou pro bunku sudoku

                1. self.i a self.j jsou indexy bunky

                2. value je hodnota bunky

                3. domain je mnozina vsech moznych hodnot pro bunku

                4. Constraints (self, sudoku, size)

                    a. Smaze z domainu hodnoty podle pravidel sudoku
                    
4. Ukázka programu.

Sudoku:
00 02 03 00 
01 00 00 00 
00 00 00 03 
00 01 02 00 

CSP solution:
04 02 03 01 
01 03 04 02 
02 04 01 03 
03 01 02 04 

Time: 0.00037288665771484375
SAT solution:
04 02 03 01 
01 03 04 02 
02 04 01 03 
03 01 02 04 

Time: 0.025455236434936523
Sudoku:
08 00 00 00 00 00 00 00 00 
00 00 03 06 00 00 00 00 00 
00 07 00 00 09 00 02 00 00 
00 05 00 00 00 07 00 00 00 
00 00 00 00 04 05 07 00 00 
00 00 00 01 00 00 00 03 00 
00 00 01 00 00 00 00 06 08 
00 00 08 05 00 00 00 01 00 
00 09 00 00 00 00 04 00 00 

CSP solution:
08 01 02 07 05 03 06 04 09 
09 04 03 06 08 02 01 07 05 
06 07 05 04 09 01 02 08 03 
01 05 04 02 03 07 08 09 06 
03 06 09 08 04 05 07 02 01 
02 08 07 01 06 09 05 03 04 
05 02 01 09 07 04 03 06 08 
04 03 08 05 02 06 09 01 07 
07 09 06 03 01 08 04 05 02 

Time: 8.72238302230835
SAT solution:
08 01 02 07 05 03 06 04 09 
09 04 03 06 08 02 01 07 05 
06 07 05 04 09 01 02 08 03 
01 05 04 02 03 07 08 09 06 
03 06 09 08 04 05 07 02 01 
02 08 07 01 06 09 05 03 04 
05 02 01 09 07 04 03 06 08 
04 03 08 05 02 06 09 01 07 
07 09 06 03 01 08 04 05 02 

Time: 0.13523101806640625
Sudoku:
10 09 12 00 00 00 07 00 03 06 00 15 14 02 01 00 
01 00 00 15 00 00 00 00 00 00 13 00 00 06 07 16 
00 00 00 00 00 12 00 00 00 01 14 00 00 00 09 04 
00 00 00 11 00 00 00 00 00 05 04 09 03 00 10 15 
00 07 00 00 00 01 00 00 13 12 00 00 00 11 00 00 
11 00 04 06 09 03 00 00 00 00 07 00 00 00 15 01 
00 00 10 00 14 05 02 00 09 00 00 11 00 13 00 08 
00 13 00 03 06 00 08 11 00 15 00 14 12 10 00 00 
00 00 02 01 07 00 11 00 04 13 00 06 00 00 08 00 
08 00 13 00 12 00 05 00 00 09 03 02 00 04 00 00 
07 04 00 00 00 14 00 09 00 00 05 16 15 03 00 13 
00 00 11 00 00 00 00 15 00 00 10 00 00 00 16 00 
06 10 00 05 13 09 14 00 00 00 00 00 08 16 00 00 
16 00 00 00 00 02 06 00 14 00 09 00 04 05 00 00 
03 00 00 00 00 15 00 00 00 00 00 00 01 00 00 00 
00 12 14 13 16 00 10 08 00 07 00 00 00 15 06 03 

CSP solution:
10 09 12 04 11 08 07 13 03 06 16 15 14 02 01 05 
01 05 03 15 10 04 09 14 02 08 13 12 11 06 07 16 
02 06 07 16 15 12 03 05 11 01 14 10 13 08 09 04 
13 14 08 11 01 06 16 02 07 05 04 09 03 12 10 15 
14 07 16 02 04 01 15 10 13 12 08 03 06 11 05 09 
11 08 04 06 09 03 13 12 10 02 07 05 16 14 15 01 
15 01 10 12 14 05 02 16 09 04 06 11 07 13 03 08 
09 13 05 03 06 07 08 11 16 15 01 14 12 10 04 02 
12 16 02 01 07 10 11 03 04 13 15 06 05 09 08 14 
08 15 13 14 12 16 05 06 01 09 03 02 10 04 11 07 
07 04 06 10 02 14 01 09 08 11 05 16 15 03 12 13 
05 03 11 09 08 13 04 15 12 14 10 07 02 01 16 06 
06 10 01 05 13 09 14 07 15 03 12 04 08 16 02 11 
16 11 15 07 03 02 06 01 14 10 09 08 04 05 13 12 
03 02 09 08 05 15 12 04 06 16 11 13 01 07 14 10 
04 12 14 13 16 11 10 08 05 07 02 01 09 15 06 03 

Time: 6.685417175292969

SAT solution:
10 09 12 04 11 08 07 13 03 06 16 15 14 02 01 05 
01 05 03 15 10 04 09 14 02 08 13 12 11 06 07 16 
02 06 07 16 15 12 03 05 11 01 14 10 13 08 09 04 
13 14 08 11 01 06 16 02 07 05 04 09 03 12 10 15 
14 07 16 02 04 01 15 10 13 12 08 03 06 11 05 09 
11 08 04 06 09 03 13 12 10 02 07 05 16 14 15 01 
15 01 10 12 14 05 02 16 09 04 06 11 07 13 03 08 
09 13 05 03 06 07 08 11 16 15 01 14 12 10 04 02 
12 16 02 01 07 10 11 03 04 13 15 06 05 09 08 14 
08 15 13 14 12 16 05 06 01 09 03 02 10 04 11 07 
07 04 06 10 02 14 01 09 08 11 05 16 15 03 12 13 
05 03 11 09 08 13 04 15 12 14 10 07 02 01 16 06 
06 10 01 05 13 09 14 07 15 03 12 04 08 16 02 11 
16 11 15 07 03 02 06 01 14 10 09 08 04 05 13 12 
03 02 09 08 05 15 12 04 06 16 11 13 01 07 14 10 
04 12 14 13 16 11 10 08 05 07 02 01 09 15 06 03 

Time: 0.6679818630218506
