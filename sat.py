import pycosat

def solve(sudoku, size):
    clauses = sudokuClauses(size)
    initClauses(clauses, sudoku, size)
    solution = set(pycosat.solve(clauses))
    return getSolved(sudoku, solution, size)

def sudokuClauses(size):
    clauses = []
    haveValueClauses(clauses, size)
    rowsClauses(clauses, size)
    colsClauses(clauses, size)
    areaClauses(clauses, size)
    return clauses

def haveValueClauses(clauses, size):
    for i in range(0, size**2):
        for j in range(0, size**2):
            values = []
            for value in range(1, size**2 + 1):
                values.append(getLogicalVariable(i, j, value))
            clauses.append(values)
            for value in range(1, size**2 + 1):
                for value_1 in range(value + 1, size**2 + 1):
                    clauses.append([-getLogicalVariable(i, j, value), \
                                    -getLogicalVariable(i, j, value_1)])

def getLogicalVariable(i, j, value):
    var = "{0:0=2d}".format((i + 1)) + "{0:0=2d}".format((j + 1)) + \
                                                "{0:0=2d}".format(value)
    return int(var)

def rowsClauses(clauses, size):
    for i in range(0, size ** 2):
        rows = []
        for j in range(0, size**2):
            rows.append((i, j))
        valid(rows, clauses, size)

def colsClauses(clauses, size):
    for i in range(0, size ** 2):
        cols = []
        for j in range(0, size ** 2):
            cols.append((j, i))
        valid(cols, clauses, size)

def areaClauses(clauses, size):
    z = [i for i in range(0, size ** 2) if i % size == 0]
    for i in z:
        for j in z:
            areas = []
            for k in range(0, size ** 2):
                areas.append((i + k % size, j + k // size))
            valid(areas, clauses, size)

def valid(cells, clauses, size):
    for i, xi in enumerate(cells):
        for j, xj in enumerate(cells):
            if i < j:
                for value in range(1, size**2 + 1):
                    clauses.append([-getLogicalVariable(xi[0], xi[1], value),\
                                    -getLogicalVariable(xj[0], xj[1], value)])

def initClauses(clauses, sudoku, size):
    for i in range(0, size**2):
        for j in range(0, size**2):
            value = sudoku[i][j]
            if value:
                clauses.append([getLogicalVariable(i, j, value)])

def getValue(i, j, solution, size):
    for value in range(1, size**2 + 1):
        if getLogicalVariable(i, j, value) in solution:
            return int(value)

def getSolved(sudoku, solution, size):
    for i in range(0, size**2):
        for j in range(0, size**2):
            sudoku[i][j] = getValue(i, j, solution, size)
    return sudoku
