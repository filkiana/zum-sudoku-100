import CSPSudokuCell
import copy

def solve(sudoku, size):
    return DFS(sudoku, size)

def initCsp(sudoku, size):
    csp_sudoku = []
    for i in range(0, size ** 2):
        csp_sudoku.append([])
        for j in range(0, size ** 2):
            csp_sudoku[i].append(CSPSudokuCell.CSPSudokuCell(i, j, size, \
                                                             sudoku[i][j]))
    return csp_sudoku

def controlContraints(csp_sudoku, size):
    k = False
    while (k == False):
        k = True
        for i in range(0, size ** 2):
            for j in range(0, size ** 2):
                z = csp_sudoku[i][j].Constraints(csp_sudoku, size)
                if z != True:
                    k = False

def DFS(sudoku, size):
    current = initCsp(sudoku, size)
    stack = []
    stack.append(current)
    while(len(stack) != 0):
        current = stack.pop()
        controlContraints(current, size)
        if isError(current):
            continue
        if isSolution(current, size):
            return current
        #PrintCSPSudoku(current, size)

        i, j = findLeast(current, size)
        for k in current[i][j].domain:
            leaf = copy.deepcopy(current)
            leaf[i][j].domain = [k]
            leaf[i][j].value = k
            stack.append(leaf)
    return False

def findLeast(csp_sudoku, size):
    index_i = 0
    index_j = 0
    minlen = 10
    for i in range(0, size**2):
        for j in range(0, size**2):
            if((minlen > len(csp_sudoku[i][j].domain)) and \
               (len(csp_sudoku[i][j].domain) >= 2)):
                    minlen = len(csp_sudoku[i][j].domain)
                    index_i = i
                    index_j = j
                    if minlen == 2:
                        return i, j
    return index_i, index_j

def isSolution(csp_sudoku, size):
    z = size**2 * (1 + size**2)/2 * size**2
    for i in csp_sudoku:
        for j in i:
            z = z - j.value
    if z == 0:
        return True
    else:
        return False

def printCSPSudoku(csp_sudoku, size):
    for i in range(0, size ** 2):
        for j in range(0, size ** 2):
            print("{0:0=2d}".format(csp_sudoku[i][j].value), end =' ')
        print()
    print()

def printSudoku(sudoku, size):
    for i in range(0, size ** 2):
        for j in range(0, size ** 2):
            print("{0:0=2d}".format(sudoku[i][j]), end=' ')
        print()
    print()

def isError(csp_sudoku):
    for i in csp_sudoku:
        for j in i:
            if j.value == 0 and len(j.domain) == 0:
                return True
    return False
