class CSPSudokuCell(object):
    def __init__(self, i, j, size, init_val):
        self.value = init_val
        self.i = i
        self.j = j
        if init_val == 0:
            self.domain = [i for i in range(1, size**2 + 1)]
        else:
            self.domain = [init_val]

    def Constraints(self, sudoku, size):
        ret = True
        for k in range(0, size**2):
            if sudoku[k][self.j].value != 0:
                if sudoku[k][self.j].value in self.domain:
                    self.domain.pop(self.domain.index(sudoku[k][self.j].value))
                    ret = False
            if sudoku[self.i][k].value != 0:
                if sudoku[self.i][k].value in self.domain:
                    self.domain.pop(self.domain.index(sudoku[self.i][k].value))
                    ret = False
        a = int(self.i / size)
        b = int(self.j / size)
        for m in range(a * size, a * size + size):
            for n in range(b * size, b * size + size):
                if sudoku[m][n].value != 0:
                    if sudoku[m][n].value in self.domain:
                        self.domain.pop(self.domain.index(sudoku[m][n].value))
                        ret = False
        if len(self.domain) == 1:
            self.value = self.domain[0]
        return ret
